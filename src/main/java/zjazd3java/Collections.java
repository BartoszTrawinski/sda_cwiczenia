package zjazd3java;

import java.util.*;

public class Collections {
  public static void main(String[] args) {
    List<String> lista = new ArrayList<>();
    lista.add("Bartus");

    //pobiera element z listy
    lista.get(0);

    //długość listy
    lista.size();

    //usuń element z listy
    lista.remove(0);

    //Sprawdzic na ktorej pozycji znajduje się okreslony element
    int x = lista.indexOf("Bartus");

    // szuka w liście czy  jest w liście/kolekcji
    boolean exists = lista.contains("Bartus");

    // wyszukać danego Stringa jesli ich jest wiecej niż dwa

    int counter = 0;
    for (int i = 0; i < lista.size(); i++) {
      if(lista.get(i).equals("Bartus"))
        counter++;
    }
    //generowanie liczby
    Random random = new Random();
    // liczba od 1-10
    int number1 = random.nextInt(11);
    //liczby od 10 do 20
    int number2 = random.nextInt(11) + 10;

    //kolejka
    PriorityQueue<Integer> queue = new PriorityQueue<>();
    Queue<Integer> queueSecond = new PriorityQueue<>();
    // dodaac element
    queue.add(11);
    //pobrac element z kolejki i  pozostawic
    Integer value = queue.peek();
    //pobrac z kolejki i usunąć
    Integer value1 = queue.poll();
    //ilosc elementow w kolejce
    Integer numbersOfElementsInQueue = queue.size();
  }
}
