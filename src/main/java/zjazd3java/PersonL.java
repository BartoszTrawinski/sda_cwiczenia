package zjazd3java;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

//extends -  rozszerza o dodatkową klasę (dziedziczenie) (jesli klasa abstrakcyjna trzeba rozszerzyć)

public class PersonL extends BasePerson implements PersonIdentity{
    public static final String NATIONALITY = "PL";
    private String pesel;
    private String name;
    private String surname;
    private LocalDateTime dateOfBirth;
    private String eyeColor;
    private char sex;

    @Override
    public void display() {
        this.toString();
    }

    @Override
    public String getIdentity() {
        return pesel;
    }
}

