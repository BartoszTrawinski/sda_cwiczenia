package zjazd3java;

import java.time.LocalDateTime;

public class Main {
  public static void main(String[] args) {
    Person person = new Person();
    System.out.println(person.toString());
    Person person2 = new Person("Grzegorz", "Brzęczyszczykiewicz");
    System.out.println(person2.toString());

    System.out.println("Imię osoby to; " + person2.getName());
    System.out.println("Nazwisko osoby to " + person2.getSurname());

    person2.setName("Jan");
    System.out.println("Imię osoby to; " + person2.getName());
    Person person3 = new Person("Bartosz", "Trawiński", LocalDateTime.now(), "zielony",'M');
    System.out.println("Nowa osoba " + person3.toString());

    PersonL personL = new PersonL();
    System.out.println(personL.toString());

    // ctrlL i najezdzamy na metode przenosi nas do klasy gdzie ją opisaliśmy
    personL.display();
    personL.calculateBMI();
    personL.getHeight();
    personL.getWeight();

  }
}
