package zjazd2java.car;

public class Car {
    public final String id;
    public final int power;
    public final double capacityFuel;
    private double levelFuel;


    public Car(String id, int power, double capacityFuel){
        this.id = id;
        this.power = power;
        this.capacityFuel = capacityFuel;
    }

    public double refuel (double level) {
        if (capacityFuel >= level + levelFuel) {
            levelFuel = levelFuel + level;
            return 0;
        } else {
            double diff = capacityFuel - level;
            levelFuel = capacityFuel;
            return level - diff;
        }
    }
    public double getLevelFuel() {
        return levelFuel;
    }

   /* public string getId (){
        return id;
    }
    public int getPower (){
        return power;
*/
    public String toString() {
        return "Numer rejestracyjny Twojego auta to " + id + "a jego moc silnika to "+ power + "cm3";
    }
}
