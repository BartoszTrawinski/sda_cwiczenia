package zjazd2java.car;

import zjazd2java.car.Car;

public class CarDemo {
    public static void main(String[] args) {
        Car auto = new Car ("CZN 15645", 1695, 45);
        System.out.println("zostało po tankowaniu " + auto.refuel(10));
        System.out.println("poziom paliwa " + auto.getLevelFuel());
        System.out.println("zostało po tankowaniu " + auto.refuel(70));
        System.out.println("poziom paliwa " + auto.getLevelFuel());

    }
}
