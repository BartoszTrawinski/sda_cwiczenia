package zjazd2java.string;

public class StringDemo {
  public static void main(String[] args) {
    // metoda do tworzenia stringow i
    String city = "Bydgoszcz";
    String home1 = "Bydgoszcz";
    // druga metoda tworzenia stringow
    String home2 = new String("Bydgoszcz");
    // tu pokazuje rowne bo wskazuja na ten sam łańcuch w puli łańcuchów

    //tu referencje są różne bo home2 to obiekt

    System.out.println(city == home2);
    // porównujemy przez equals

    System.out.println(city.equals(home1));
    System.out.println(city.equals(home2));
    // porównanie długości łańcuchów

    System.out.println(city.length() == home1.length());
    //zmień pierwszą literę w city

    System.out.println(city.charAt(0));
    // odcinamy podłańcuch od drugiego znaku do końca

    System.out.println(city.substring(1));
    // sklejamy odciety podłancuh z innym znakiem

    city = "b" + city.substring(1);
    System.out.println(city);
    city.replace("b", "B");
    System.out.println(city);
    System.out.println(city.toUpperCase());

    // zmień wielkość liter jak w nazwie własnej Pierwesze litery sa wielkimi literami
    String napis = "programowanie w java";
    String[] words = napis.split(" ");
    for (String s: words) {
      System.out.println(s);
    }
    napis = "";
    for (int i = 0; i < words.length; i++) {
      String word = words[i];
      word = (word.charAt(0) + "").toUpperCase() + word.substring(1);
      if (i == words.length - 1) {
        napis += word;
      } else {
        napis += word + " ";
      }

    }
    System.out.println(napis);

    String napis1 = "count of NUMBERS";

    // zamień napis na styl camelCase czyli countOfNumbers
    napis1 = napis1.toLowerCase();
    String[] words1 = napis1.split(" ");
    napis1 ="";

    for (int i = 0; i < words1.length; i++) {
      String word1 = words1 [i];
      if(i == 0){
        napis1 = word1;
        continue;
      }
      napis1 += (word1.charAt(0)+"").toUpperCase() + word1.substring(1);

    }
    System.out.println(napis1);

  }
}
