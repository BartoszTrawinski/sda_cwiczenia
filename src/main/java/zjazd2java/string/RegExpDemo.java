package zjazd2java.string;

public class RegExpDemo {
  public static void main(String[] args) {
    String str = "Ala";
    //znajdz w łancuchu jest liter duża A a potem cokolwiek
    // . - dowolny znak
    // * - zero lub doowlna liczba znaków

    System.out.println(str.matches("A.*"));
    System.out.println("Beata".matches("A.*"));

    //+ - jeden lub wiele poprzedzających znaków

    System.out.println("Beata".matches(".*a"));

    //czy łańcuch składa sie z liter a
    // z uwzglednieniem wielkości znajów

    str = "aaaaAaaaa";
    System.out.println(str.matches("a*"));

    // bez uwzgęlniania wielkosći znaków
    //?i - flaga, ignoruje wielkość znaków

    System.out.println(str.matches("(?i)a*"));

    // czy znajduja sie liter mała litera i dowolna litera b/B

    str = "aaaaaaBBBbbb";
    System.out.println(str.matches("a*(?i)b*"));
    // zbiór zbaków - [a-z],[afr],[a-zA-Z]
    // liczba powtorzeń - {n},{n,m} od n do m
    // czy pierwszy znak jest cyfrą

    str = "lkhkgfdlajd";
    System.out.println(str.matches("[0-9]{1}.*"));
    // znaki specjalne
    // \d - dowolna cyfra
    // \D - dowolny znak który nie jest cyfrą
    System.out.println(str.matches("\\d.*"));
    //sprawdz czy w łancuch znajduje się kod pocztowy
    str = "88-192";
    System.out.println(str.matches("\\d,\\d-\\d,\\d,\\d"));
    System.out.println(str.matches("\\d{2}-\\d{3}"));
    System.out.println(str.matches("[0-9]{2}-[0-9]{3}"));
    // sprawdz czy w łancuchu znajduje sie liczba dwucyfrowa, mniejsza od 50
    str = "64";
    System.out.println(str.matches("[1-4]\\d"));
    // \b - granice słowa
    // \B - poza granica słowa
    // \s - znak biały: spacja, tabulator itd
    // \w - znak słowa
    // \W - inny znak niż słowa
    // Czy drugie słowo zaczyna się literą b
    str = "może być";
    System.out.println(str.matches("\\b.*\\b\\bb.*\\b"));
    System.out.println(str.matches(".*\\s+b.*"));
    System.out.println(str.matches("\\b\\w+\\b\\s+b.*"));
  }
}
