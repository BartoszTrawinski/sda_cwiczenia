package zjazd2java.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceDemo {
  public static void main(String[] args) {
    String text = "Programowanie w Java";
    System.out.println(text.replaceAll("\\b\\w\\b", "z"));
    //wyszuakc i zamienic słowo 4literowe
    System.out.println(text.replaceAll("\\b\\w{4}\\b","kutasek"));
    // zamień nie na tak
    // ? dokładnie jeden dowolny znak
    System.out.println(text.replaceAll("nie", "tak"));
    // odwołanie wsteczne \numer_grupy
    text = "kod kod pocztowy";
    System.out.println(text.replaceAll("(\\b\\w+\\b)\\s+\\1",""));
    // przy pomocy pattern
    Pattern pat = Pattern.compile("(\\b\\w+\\b)\\s+(\\1).*");
    Matcher mat = pat.matcher(text);
    System.out.println(mat.matches());
    System.out.println(mat.group(1));
    System.out.println(mat.group(2));
  }
}
