package zjazd2java.string;

public class StringiBuilderDemo {
  public static void main(String[] args) {
    //szybsza
    StringBuilder builder = new StringBuilder();
    //wolniejsza
    StringBuffer buffer = new StringBuffer();
    builder.append("abc");
    System.out.println(builder);
    // dodać resztę alfabetu
    for (char c = 'd'; c <= 'z'; c++) {
      builder.append(c);
    }
    System.out.println(builder);

    builder.deleteCharAt(0);
    builder.deleteCharAt(builder.length()-1);
    System.out.println(builder);

    // 0 .. 9 A B C....Z
    builder.delete(0, builder.length());
    for (char c = '0'; c <= '9'; c++) {
      builder.append(c);
    }
    for (char c = 'A'; c <= 'F'; c++) {
      builder.append(c);
    }
    System.out.println(builder);
  }
}
