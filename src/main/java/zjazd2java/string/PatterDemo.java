package zjazd2java.string;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatterDemo {
  public static void main(String[] args) {
    Pattern pattern = Pattern.compile("\\b\\w+\\b\\s+b.*", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CHARACTER_CLASS);
    Matcher matcher = pattern.matcher("może być");
    System.out.println(matcher.matches());
    // grupy () - maksymalnie może być od 0 do 9

    //wyłuskiwanie z uzyciem find() -całe wyrażenie jest szukane

    Pattern kod = Pattern.compile("\\d\\d-\\d\\d\\d");
    Matcher kodMatcher = kod.matcher("Kod pocztowy: 88-192");
    if (kodMatcher.find()) {
      System.out.println(kodMatcher.group());
    }

    // wyłuskianiwe z uzyciem matches
    kod = Pattern.compile(".*:\\s(\\d\\d)-(\\d\\d\\d)");
    kodMatcher = kod.matcher("Kod pocztowy: 88-192");
    if (kodMatcher.matches()) {
      System.out.println(kodMatcher.groupCount());
      int n = 1;
      while (kodMatcher.groupCount() >= n) {
        System.out.println(kodMatcher.group(n));
        n++;
      }
    }
  }
}