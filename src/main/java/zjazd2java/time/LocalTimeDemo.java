package zjazd2java.time;

import java.time.LocalTime;

public class LocalTimeDemo {
  public static void main(String[] args) {
    LocalTime currentTime = LocalTime.now();
    System.out.println ((currentTime.getNano() - LocalTime.of(0, 0, 0).getNano())/10E9 );
    System.out.println (currentTime.toSecondOfDay());
    //jak długo trwaja zajecie
    LocalTime start = LocalTime.of(9,0,0);
    LocalTime end = LocalTime.of(16,30,0);
    System.out.println(end.toSecondOfDay()-start.toSecondOfDay());
    // ile zostało do konca
    System.out.println(end.toSecondOfDay() - LocalTime.now().toSecondOfDay());
    // jaka bedzie czas po upływie 1200s
    System.out.println(currentTime.plusSeconds(1200));
    // jaki bedzie czas za 2h 23min
    System.out.println(currentTime.plusHours(2).plusMinutes(23));
    // ile kosztuje rozmowa trwająca 3min 25 sek jesli opłata wynosi 5gr za każde rozpoczete 10sekund
    int seconds = LocalTime.of(0,3,25).toSecondOfDay();
    System.out.println("liczba sekund: " + seconds);
    System.out.println("liczba pełnych 10 sekund jednostek rozmowy " +seconds/10);
    System.out.println("czas trwania rozpoczętej jednostki: "+seconds%10);
    System.out.println((seconds/10)*5 + (seconds%10 > 0 ? 5:0));
  }
}


