package zjazd2java.time;

import java.time.LocalDate;

public class LocacDateDemo {
  public static void main(String[] args) {
    LocalDate date = LocalDate.now();
    LocalDate birth = LocalDate.of(1985,3,23);
    System.out.println(date);
    System.out.println(birth);
    // ile dni upłyneło miedzy dzisiejsza a a urodzeniem
    System.out.println(date.toEpochDay() - birth.toEpochDay());
    // jaka bedzie data za 100dni
    System.out.println(date.plusDays(100));


    //za ile dni mam urodziny
    // sprawdzamy czy miesiac biezacy jest mniejszy od miesiaca urodzin
    // data urodzin w bierzacym roku
    LocalDate currentBirth = LocalDate.of(date.getYear(),birth.getMonth(),birth.getDayOfMonth());
    if (date.isBefore(currentBirth)) {
      // jesli urodziny sa w bierzacym roku
      System.out.println(currentBirth.toEpochDay() - date.toEpochDay());
    } else {
      // jesi urodziny sa w kolejnym roku
      LocalDate nextBirth = currentBirth.plusYears(1);
      System.out.print(nextBirth.toEpochDay() - date.toEpochDay());
    }
    System.out.println(" dni");
    // jaki miesiac bedzie za 3000dni
    System.out.println(LocalDate.now().plusDays(3000).getMonth());
  }

}
