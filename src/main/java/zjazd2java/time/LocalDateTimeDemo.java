package zjazd2java.time;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeDemo {
  public static void main(String[] args) {
    LocalDateTime timeStamp = LocalDateTime.now();
    System.out.println(timeStamp);
    System.out.println(timeStamp.toLocalDate()+" "+timeStamp.toLocalTime());
    //jaka data i czas za 100dni
    System.out.println(timeStamp.plusDays(100));
    // ile sekund minelo od dnia z godzina narodzin
    LocalDateTime birthStramp = LocalDateTime.of(1985,3,23,4,30,0);
    System.out.println(timeStamp.toEpochSecond(ZoneOffset.MAX) - birthStramp.toEpochSecond(ZoneOffset.MAX));
  }
}
