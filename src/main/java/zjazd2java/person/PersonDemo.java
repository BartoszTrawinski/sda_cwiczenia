package zjazd2java.person;

import zjazd2java.person.Person;

public class PersonDemo {
    public static void main(String[] args) {
        Person member = new Person( "Bartek",  "Trawiński" );
        System.out.println(member);

        Person wife = new Person("Nowak","Maria", 23, 2, 1989);
        System.out.println(wife);

    }
}
