package zjazd2java.shape;

public class CircleDemo {
    public static void main(String[] args) {
        Circle circle = Circle.makeUnitCircle();
        System.out.println(circle);
        System.out.println(new Circle(5).getPerimeter());
        System.out.printf("Pole powierzchni koła wynosi %.2f", new Circle(5).getArea());
    }
}
