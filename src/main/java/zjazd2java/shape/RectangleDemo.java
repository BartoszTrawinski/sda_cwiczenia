package zjazd2java.shape;

public class RectangleDemo {
    public static void main(String[] args) {
        Rectangle rect = new Rectangle(5,8);
        Rectangle square = Rectangle.makeSquare(7);
        Rectangle box = Rectangle.makeUnitSquare();
        System.out.println(rect);
        System.out.println(square);
        System.out.println(Rectangle.count);
        System.out.println(rect.count);
        System.out.println(square.count);
        System.out.println(box);
    }
}
