package zjazd2java.shape;

public class Circle {
    int radius;

    public Circle (int radius){
        this.radius = radius;
    }
    public double getPerimeter (){
        return Math.PI * 2 * radius;
    }
    public static Circle makeUnitCircle () {
        return new Circle(1);
    }
    public double getArea (){
        return Math.PI * Math.pow(radius,2);
    }
    public String toString() {
        return "Circle " + radius;
    }
}
