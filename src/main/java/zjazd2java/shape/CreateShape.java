package zjazd2java.shape;

import java.util.Scanner;

public class CreateShape {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
    /*    System.out.println("Podaj długość prostokąta: ");
        int width = scanner.nextInt();
        System.out.println("Podaj wysokość prostokąta: ");
      int height = scanner.nextInt(); */
        System.out.println("Podaj długość i wysokość prostokąta: ");
        int a;
        int b;
        if (scanner.hasNext()){
            a = scanner.nextInt();
        } else{
            System.out.println("Niepoprawny typ danej");
            return;
        }
        if (scanner.hasNext()) {
            b = scanner.nextInt();
            if (a >= 0 && b >= 0) {
                Rectangle box = new Rectangle(scanner.nextInt(),scanner.nextInt());
                System.out.println((box.getArea()+ " " + box.getPerimeter()));

            }else {
                System.out.println("Wartości boków nie mogą być ujemne");
            }

            Rectangle box = new Rectangle(a,b);
        }else {
            System.out.println("Niepoprawny typ danej");
            return;
        }

    }
}
