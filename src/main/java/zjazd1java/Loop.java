package zjazd1java;

import java.util.Scanner;

public class Loop {
    public static void main(String[] args)  throws InterruptedException {
            forLoop();
            //whileLoop();
            //doWhileLoop();
    }
        public static void forLoop() throws InterruptedException {
            for (int i = 0; i < 10; i++) {
                Thread.sleep(1000);
                System.out.println("Liczba: " + i);
            }
        }
        public static void whileLoop() {
            int counter = 0;
            while (counter < 10){
                System.out.println("Liczba: " + counter);
                counter++;
            }
        }
        public static void doWhileLoop(){
            int counter = 0;
            do {
                System.out.println("Liczba: " + counter);
                counter++;
            } while (counter < 10);
        }

    public static class Switch {
        public static void main(String[] args) {
            System.out.print("Podaj kolor: ");
            Scanner scanner = new Scanner(System.in);
            String color = scanner.nextLine();

            switch(color){
                case "red": System.out.println("Podałeś kolor czerwony");
                    break;
                case "blue": System.out.println("Podałeś kolor niebieski");
                    break;
                case "green": System.out.println("Podałeś kolor zielony");
                    break;
                case "yellow": System.out.println("Podałeś kolor żółty");
                    break;
                default:
            }
        }
    }

    public static class UserNumbers {
        public static void main(String[] args) {
            // 1 Pobrać od użytkowanika ilosć liczb o użytkownika - scanner
            // 2 Pobrać n liczb - scanner + pętla for
            // 3 zapamiętać liczby - tablica nelementowa jednowymiarowa
            // 4 wyświetli liczby które użytkownik wprowadził. - System out + while

            Scanner scanner = new Scanner(System.in);
            System.out.print("Wprowadź ilość liczb do wprowadzenia: ");
            int amountOfNumbers = scanner.nextInt();

            int [] numbers = new int[amountOfNumbers];

            for (int i = 0; i < amountOfNumbers; i++) {
                System.out.print("Podaj liczbę " + (i+1) +": ");
                int number = scanner.nextInt();
                numbers [i] = number;
            }

            int counter = 0;
            System.out.println("Twoje liczby to: ");
            while (counter < numbers.length) {
                System.out.print(numbers[counter] + " ");
                counter++;
            }
        }
    }
}
