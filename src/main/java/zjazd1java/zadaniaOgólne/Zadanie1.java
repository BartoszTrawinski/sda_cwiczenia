package zjazd1java.zadaniaOgólne;

import java.util.Scanner;

public class Zadanie1 {
  public static void main(String[] args) {
    String slownie = " ";
    String[] jednosci = {"", " jeden", " dwa", " trzy", " czter", " pięć", " sześć", " siedem", " osiem", " dziewieć"};
    String[] nascie = {"dziesięć","jedenaście", "dwanaście", "trzynaście", "czternaście", "piętnaście", "szesnaście", "siedemnaście", "osiemnaście", "dziewiętnaście"};
    String[] dziesiatki = {"", "dziesięć","dwadzieścia", "trzydzieści", "czterdzieści", "pięćdziesiąt", "sześćdziesiąt", "siedemdziesiąt", "osiemdziesiąt", "dziewięćdziesiat"};
    int liczba, koncowka;
    int j = 0;
    System.out.println("Podaj liczbę od 1 do 99, którą chcesz zamienić na słowo: ");
    Scanner scanner = new Scanner(System.in);
    liczba = scanner.nextInt();

    while (liczba > 0) {
      koncowka = (liczba % 10);
      liczba /= 10;
      if ((j==0)&&(liczba%10!=1)) slownie = jednosci[koncowka] + slownie;
      if ((j==0)&&(liczba%10==1)){
        slownie = nascie[koncowka] + slownie;
        liczba/=10;
        j+=0;
        continue;
      }
      if (j==1) slownie = dziesiatki[koncowka] + slownie;

      j++;
    }
    System.out.println("Odpowiedz: " + slownie);

  }
}
