package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wpisz 1 liczbę a jeśli chcesz zakończyć wpisywanie liczb i wyświetlić średnią wpisz słowo (koniec): ");
        double sumaLiczb = 0;
        int i = 0;
        String tekst;

        while (!((tekst = scanner.nextLine()).equals("koniec"))) {
            System.out.println("Podaj liczbę  " + (i + 1) + ": ");
            sumaLiczb += Integer.parseInt(tekst) ;
            i++;

            double sredniaLiczb = sumaLiczb / i;
            System.out.println("Twoja średnia liczb wynosi: " + sredniaLiczb);

        }
    }
}