package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbę: ");
        Scanner scanner = new Scanner(System.in);
        int Liczba = scanner.nextInt();
        if (Liczba%2 == 0) {
            System.out.println("To jest liczba parzysta");
        } else {
            System.out.println("To jest liczba nieparzysta");
        }
    }
}
