package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj ilość liczb do wpisania: ");
        int iloscLiczb = scanner.nextInt();
        double sumaLiczb = 0;
        for (int i = 0; i < iloscLiczb; i++) {
            System.out.print("Podaj liczbę  " + (i + 1) + ": ");
            int liczba = scanner.nextInt();
            sumaLiczb += liczba;
        }
        double sredniaLiczb = sumaLiczb / iloscLiczb;
        System.out.print("Średnia Twoich liczb wynosi; " + sredniaLiczb);

    }
}