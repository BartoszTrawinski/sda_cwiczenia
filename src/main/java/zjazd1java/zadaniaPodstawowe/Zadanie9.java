package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj temperaturę w stopniach Celcjusza: ");
        double cel = scanner.nextDouble();

        double far = (cel*9)/5+32;
        System.out.print("Temperatura w stopniach Fahrenheit'a wynosi: " + far);


    }
}
