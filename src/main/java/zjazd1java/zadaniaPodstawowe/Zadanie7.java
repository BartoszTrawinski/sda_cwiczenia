package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args) {
        System.out.print("Podaj długość boku kwadratu: ");
        Scanner scanner = new Scanner(System.in);
        double bok = scanner.nextDouble();

        double pole = bok * bok;
        System.out.println("Pole powierzchni kwadratu wynosi: " + pole);
        double obwod = 4*bok;
        System.out.println("Obwód kwadratu wynosi: " + obwod);
    }
}
