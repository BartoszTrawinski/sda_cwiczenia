package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {

        int intFirstNumber = readNumber(1);
        int intSecondNumber = readNumber(2);

        int sumaNumbers = intFirstNumber + intSecondNumber;
        System.out.println("Suma liczb to: " + sumaNumbers);
        int difNumbers = intFirstNumber - intSecondNumber;
        System.out.println("Róźnica liczb to: " + difNumbers);
        int multiNumbers = intFirstNumber * intSecondNumber;
        System.out.println("Iloczyn liczb to: " + multiNumbers);
        float divNumbers = intFirstNumber / (float) intSecondNumber;
        System.out.println("Iloraz liczb to: " + divNumbers);
    }

    private static int readNumber(int counter) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj " + counter + " liczbę: ");
        String number = scanner.nextLine();
        int intNumber = 0;
        try {
            intNumber = Integer.parseInt(number);
        } catch (NumberFormatException exception) {
        }
        return intNumber;
    }
}




