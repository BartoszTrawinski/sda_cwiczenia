package zjazd1java.zadaniaPodstawowe;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj długość boku trójkąta: ");
        double bok = scanner.nextDouble();

        System.out.print("Podaj wysokość trójkąta: ");
        double wysokosc = scanner.nextDouble();

        double pole = (bok * wysokosc)/2;
        System.out.println("Pole powierzchni trójkąta to: " + pole);


    }
}
