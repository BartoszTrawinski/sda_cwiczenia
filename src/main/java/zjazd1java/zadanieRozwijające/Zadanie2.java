package zjazd1java.zadanieRozwijające;

public class Zadanie2 {
    public static void main(String[] args) {
        int [] arrayList = new int[3];
        arrayList [0] = 32;
        arrayList [1] = 54;
        arrayList [2] = 67;

        int listLength = arrayList.length;
        System.out.println("Długość listy to: " + listLength);
        int firstElement = arrayList [0];
        System.out.println("Wyświetl pierwszy element listy to: " + firstElement);
        int lastElement = arrayList [listLength-1];
        System.out.println("Wyświetl ostatni element listy to: " + lastElement);
    }
}
