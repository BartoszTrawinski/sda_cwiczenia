package zjazd1java.zadanieRozwijające;

public class Zadanie8 {
    public static void main(String[] args) {
        String name = "Olgierd";
        System.out.println("Wyświetl imię dużymi literami: " + name.toUpperCase());
        System.out.println("Wyświetl imię małymi literami: " + name.toLowerCase());
        System.out.println("Długość wprowadzonego imienia to: " + name.length());
        System.out.println("Czy Twoja pierwsza litera to 'O' " + name.startsWith("O"));
        System.out.println("Czy imie Olgierd zawiera literę 'e'?" + name.contains("e"));
        System.out.println("Imię Olgierd bez liter 'i' i 'e': " + name.replace("ie",""));

        for (int i = 0; i <name.length() ; i++) {
            System.out.println("[" + (i+1) + "]" + name.charAt(i));
        }
    }
}
