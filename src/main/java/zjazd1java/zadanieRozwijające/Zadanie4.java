package zjazd1java.zadanieRozwijające;

import java.util.ArrayList;
import java.util.List;

public class Zadanie4 {
    public static void main(String[] args) {
        List <String> arrayList = new ArrayList<>();
        arrayList.add ("Bartek");
        arrayList.add ("Weronika");
        arrayList.add ("Jakub");
        arrayList.add ("Krzysiek");
        arrayList.add ("Alicja");
        //zadanie 4b
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println ("Twoje imię to: " + arrayList.get(i));
        }
        // zadanie 4c
        for (int i = arrayList.size() - 1; i >= 0; i--) {
            System.out.println("Twoje imię to: " + arrayList.get(i));
        }
    }
}
