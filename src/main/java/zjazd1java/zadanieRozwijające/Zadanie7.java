package zjazd1java.zadanieRozwijające;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie7 {
  public static void main(String[] args) {
    int [] arrayList = new int[] {5,34,78,90,104};
    Scanner scanner = new Scanner(System.in);
    int index = -1;

    System.out.println("Poodaj indeks elementu tablicy, który chcesz zobaczyć: ");
    index = scanner.nextInt();

    //Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 6
    //	at zjazd1java.zadanieRozwijające.Zadanie7.main(Zadanie7.java:8)
    try{
      System.out.println(arrayList[index]);

    } catch (ArrayIndexOutOfBoundsException a){
      System.out.println("Pobrany element jest poza tablicą");
    }
  }
}
