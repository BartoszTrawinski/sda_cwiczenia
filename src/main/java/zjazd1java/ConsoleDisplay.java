package zjazd1java;

import java.util.Arrays;

public class ConsoleDisplay {
    public void display(String text) {
        System.out.println(text);
    }

    public static class Cwiczenia {
        public static void main(String[] args) {
            int [] intArray = new int[10];
            intArray[0] = 24;
            intArray[1] = 35;
            intArray[2] = 56;
            intArray[9] = 76;
            System.out.println("Element tablicy: " + intArray[0]);
            System.out.println("Element tablicy: " + intArray[1]);
            System.out.println("Element tablicy: " + intArray[2]);
            System.out.println("Element tablicy: " + intArray[3]);
            System.out.println("Element tablicy: " + intArray[9]);

            Integer[] intArray2 = new Integer[10];
            intArray2[0] = 100;
            System.out.println("Element tablicy: " + intArray2[0]);
            System.out.println("Element tablicy: " + intArray2[1]);
            int arrayLength = intArray2.length;
            System.out.println("Długość tablicy intArray2 wynosi: " + arrayLength);

            System.out.println("Długość tablicy intArray2 wynosi: " + intArray2.length);
            intArray2[0] = intArray2[0] +2;
            System.out.println("Element tablic: " + intArray2[0]);
            intArray2[0] += 2;
            System.out.println("Element tablic: " + intArray2[0]);

            // tablica wielowymiarowa
            int [][] intArray3 = new int[10][10];
            intArray3 [0][0] = 1010;
            intArray3 [9][9] = 2345;
            System.out.println("Element tablicy: " + intArray3[0][0]); // wyświetlenie wartości

            int[] sumArray = new int[3];
            sumArray[0] = 1;
            sumArray[1] = 46;
            sumArray[2] = sumArray[0] + sumArray[1];
            System.out.println("Suma elementó tablicy wynosi: " + sumArray[2]);
            System.out.println(Arrays.toString(sumArray)); // wyświetli elemnty tablicy

            //kopiowanie tablicy
            int [] arrayFirst = new int []{1,2,3};
            int [] arraySecond = new int[10];
            arraySecond = Arrays.copyOfRange(arrayFirst, 0,3);
            System.out.println(Arrays.toString(arraySecond));

            int [] arrayThird = new int [10];
            System.arraycopy(arrayFirst, 0, arrayThird,0, arrayFirst.length);
            System.out.println(Arrays.toString(arrayThird));
        }
    }
}
