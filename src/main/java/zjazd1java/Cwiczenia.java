package zjazd1java;

import java.util.Arrays;
import java.util.Scanner;

public class Cwiczenia {
    public static void main(String[] args) {
        int [] intArray = new int[10];
        intArray[0] = 24;
        intArray[1] = 35;
        intArray[2] = 56;
        intArray[9] = 76;
        System.out.println("Element tablicy: " + intArray[0]);
        System.out.println("Element tablicy: " + intArray[1]);
        System.out.println("Element tablicy: " + intArray[2]);
        System.out.println("Element tablicy: " + intArray[3]);
        System.out.println("Element tablicy: " + intArray[9]);

        Integer[] intArray2 = new Integer[10];
        intArray2[0] = 100;
        System.out.println("Element tablicy: " + intArray2[0]);
        System.out.println("Element tablicy: " + intArray2[1]);
        int arrayLength = intArray2.length;
        System.out.println("Długość tablicy intArray2 wynosi: " + arrayLength);

        System.out.println("Długość tablicy intArray2 wynosi: " + intArray2.length);
        intArray2[0] = intArray2[0] +2;
        System.out.println("Element tablic: " + intArray2[0]);
        intArray2[0] += 2;
        System.out.println("Element tablic: " + intArray2[0]);

        // tablica wielowymiarowa
        int [][] intArray3 = new int[10][10];
        intArray3 [0][0] = 1010;
        intArray3 [9][9] = 2345;
        System.out.println("Element tablicy: " + intArray3[0][0]); // wyświetlenie wartości

        int[] sumArray = new int[3];
        sumArray[0] = 1;
        sumArray[1] = 46;
        sumArray[2] = sumArray[0] + sumArray[1];
        System.out.println("Suma elementó tablicy wynosi: " + sumArray[2]);
        System.out.println(Arrays.toString(sumArray)); // wyświetli elemnty tablicy

        //kopiowanie tablicy
        int [] arrayFirst = new int []{1,2,3};
        int [] arraySecond = new int[10];
        arraySecond = Arrays.copyOfRange(arrayFirst, 0,3);
        System.out.println(Arrays.toString(arraySecond));

        int [] arrayThird = new int [10];
        System.arraycopy(arrayFirst, 0, arrayThird,0, arrayFirst.length);
        System.out.println(Arrays.toString(arrayThird));
    }

    public static class Loop {
        public static void main(String[] args)  throws InterruptedException {
                //forLoop();
                whileLoop();
                //doWhileLoop();
        }
            public static void forLoop() throws InterruptedException {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(1000);
                    System.out.println("Liczba: " + i);
                }
            }
            public static void whileLoop() {
                int counter = 0;
                while (counter < 10){
                    System.out.println("Liczba: " + counter);
                    counter++;
                }
            }
            public static void doWhileLoop(){
                int counter = 0;
                do {
                    System.out.println("Liczba: " + counter);
                    counter++;
                } while (counter < 10);
            }
    }

    public static class Main {
        public static void main(String[] args) {
            ConsoleDisplay consoleDisplay = new ConsoleDisplay();
            consoleDisplay.display("Hello World");

            String name = "Bartosz";
            String surname = "Trawiński";
            String nameAndSurname = name + surname;
            System.out.println(nameAndSurname);

            String nameAndSurnameWithSpace = name + " " + surname;
            System.out.println(nameAndSurnameWithSpace);
            System.out.printf("%s %s", name, surname);

            Scanner scanner = new Scanner(System.in);
            System.out.print("\nWprowadź imię: ");
            name = scanner.nextLine();
            System.out.println("Wprowadzone imię: " + name);
            System.out.println("Długość wprowadzonego imienia to: " + name.length());
    //        name.length()
            String first3Chars = name.substring(0,3);
            System.out.println("Pierwsze 3 znaki to: " + first3Chars);
            System.out.println("Pierwsza litera imienia to: " + name.charAt(0));
            System.out.println("Ostatnia litera imienia to: " + name.charAt(name.length() - 1));

        }
    }
}
