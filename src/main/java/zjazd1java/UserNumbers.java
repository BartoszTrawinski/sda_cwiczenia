package zjazd1java;

import java.util.Scanner;

public class UserNumbers {
    public static void main(String[] args) {
        // 1 Pobrać od użytkowanika ilosć liczb o użytkownika - scanner
        // 2 Pobrać n liczb - scanner + pętla for
        // 3 zapamiętać liczby - tablica nelementowa jednowymiarowa
        // 4 wyświetli liczby które użytkownik wprowadził. - System out + while

        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź ilość liczb do wprowadzenia: ");
        int amountOfNumbers = scanner.nextInt();

        int [] numbers = new int[amountOfNumbers];

        for (int i = 0; i < amountOfNumbers; i++) {
            System.out.print("Podaj liczbę " + (i+1) +": ");
            int number = scanner.nextInt();
            numbers [i] = number;
        }

        int counter = 0;
        System.out.println("Twoje liczby to: ");
        while (counter < numbers.length) {
            System.out.print(numbers[counter] + " ");
            counter++;
        }
    }
}
